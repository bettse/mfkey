# MFKEY

[mfkey](https://github.com/RfidResearchGroup/proxmark3/tree/master/tools/mfkey) tools wrapped in node

### Example
```js

//const {MFKEY32, MFKEY32V2, MFKEY64} = require('mfkey');
const {MFKEY32V2} = require('mfkey');
const HEX = 0x10;

//const key = MFKEY32V2(uid, nt, nr_0, ar_0, nt1, nr_1, ar_1);
const key = MFKEY32V2(
    0x12345678, 0x1AD8DF2B, 0x1D316024, 0x620EF048, 0x30D6CB07, 0xC52077E2, 0x837AC61A
)

// Key comes back as an int, so you'll probably want to convert to hex and pad it for most use
const hexKey = key.toString(HEX).padStart(12, '0');

console.log({hexKey})

```
