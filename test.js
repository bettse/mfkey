const {MFKEY32, MFKEY32V2, MFKEY64} = require('./index');
const HEX = 0x10;

function testMFKEY32() {
  //::        <uid>    <nt>     <nr_0>   <ar_0>   <nr_1>   <ar_1>
  const key = MFKEY32(
    0x52B0F519, 0x5417D1F8, 0x4D545EA7, 0xE15AC8C2, 0xDAC1A7F4, 0x5AE5C37F
  ).toString(HEX).padStart(12, '0');
  console.log({key})
}

function testMFKEY32V2() {
  //::          <uid>    <nt>     <nr_0>   <ar_0>   <nt1>    <nr_1>   <ar_1>
  const key1 = MFKEY32V2(
    0x12345678, 0x1AD8DF2B, 0x1D316024, 0x620EF048, 0x30D6CB07, 0xC52077E2, 0x837AC61A
  ).toString(HEX).padStart(12, '0');
  const key2 = MFKEY32V2(
    0x52B0F519, 0x5417D1F8, 0x4D545EA7, 0xE15AC8C2, 0xA1BA88C6, 0xDAC1A7F4, 0x5AE5C37F
  ).toString(HEX).padStart(12, '0');

  console.log({key1, key2})
}

function testMFKEY64() {
  //::        <uid>    <nt>     <nr>     <ar>     <at>
  const key1 = MFKEY64(
    0x9C599B32, 0x82A4166C, 0xA1E458CE, 0x6EEA41E0, 0x5CADF439
  ).toString(HEX).padStart(12, '0');

  const key2 = MFKEY64(
    0x52B0F519, 0x5417D1F8, 0x4D545EA7, 0xE15AC8C2, 0x5056E41B
  ).toString(HEX).padStart(12, '0');

  const key3 = MFKEY64(
    0x14579f69, 0xce844261, 0xf8049ccb, 0x0525c84f, 0x9431cc40
  ).toString(HEX).padStart(12, '0');

  console.log({key1, key2, key3})
}

testMFKEY32();
testMFKEY32V2();
testMFKEY64();
