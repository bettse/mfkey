#include <nan.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include "crapto1.h"
#include "util_posix.h"

#ifdef __cplusplus
} // extern "C"
#endif

using v8::Context;
using v8::Exception;
using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::Object;
using v8::String;
using v8::Number;
using v8::Value;
using v8::Function;

NAN_METHOD(MFKEY32) {
  Isolate* isolate = info.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  for(size_t i = 0; i < 6; i++) {
    if (!info[i]->IsNumber()) {
      Nan::ThrowError("Argument must be a number");
      return;
    }
  }

  uint32_t uid = info[0]->Uint32Value(context).ToChecked();
  uint32_t nt = info[1]->Uint32Value(context).ToChecked();
  uint32_t nr0_enc = info[2]->Uint32Value(context).ToChecked();
  uint32_t ar0_enc = info[3]->Uint32Value(context).ToChecked();
  uint32_t nr1_enc = info[5]->Uint32Value(context).ToChecked();
  uint32_t ar1_enc = info[6]->Uint32Value(context).ToChecked();
  struct Crypto1State *s, *t;
  uint64_t key;     // recovered key
  uint32_t ks2;     // keystream used to encrypt reader response

  // Generate lfsr successors of the tag challenge
  uint32_t p64 = prng_successor(nt, 64);

  // Extract the keystream from the messages
  ks2 = ar0_enc ^ p64;

  s = lfsr_recovery32(ar0_enc ^ p64, 0);

  for (t = s; t->odd | t->even; ++t) {
    lfsr_rollback_word(t, 0, 0);
    lfsr_rollback_word(t, nr0_enc, 1);
    lfsr_rollback_word(t, uid ^ nt, 0);
    crypto1_get_lfsr(t, &key);
    crypto1_word(t, uid ^ nt, 0);
    crypto1_word(t, nr1_enc, 1);
    if (ar1_enc == (crypto1_word(t, 0, 0) ^ p64)) {
      break;
    }
  }
  free(s);

  auto returnKey = Nan::New<v8::Number>(key);
  info.GetReturnValue().Set(returnKey);
}

NAN_METHOD(MFKEY32V2) {
  Isolate* isolate = info.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  for(size_t i = 0; i < 7; i++) {
    if (!info[i]->IsNumber()) {
      Nan::ThrowError("Argument must be a number");
      return;
    }
  }

  uint32_t uid = info[0]->Uint32Value(context).ToChecked();
  uint32_t nt0 = info[1]->Uint32Value(context).ToChecked();
  uint32_t nr0_enc = info[2]->Uint32Value(context).ToChecked();
  uint32_t ar0_enc = info[3]->Uint32Value(context).ToChecked();
  uint32_t nt1 = info[4]->Uint32Value(context).ToChecked();
  uint32_t nr1_enc = info[5]->Uint32Value(context).ToChecked();
  uint32_t ar1_enc = info[6]->Uint32Value(context).ToChecked();

  struct Crypto1State *s, *t;
  uint64_t key;     // recovered key
  uint32_t ks2;     // keystream used to encrypt reader response

  // Generate lfsr successors of the tag challenge
  uint32_t p64 = prng_successor(nt0, 64);
  uint32_t p64b = prng_successor(nt1, 64);

  // Extract the keystream from the messages
  ks2 = ar0_enc ^ p64;

  s = lfsr_recovery32(ar0_enc ^ p64, 0);

  for (t = s; t->odd | t->even; ++t) {
    lfsr_rollback_word(t, 0, 0);
    lfsr_rollback_word(t, nr0_enc, 1);
    lfsr_rollback_word(t, uid ^ nt0, 0);
    crypto1_get_lfsr(t, &key);

    crypto1_word(t, uid ^ nt1, 0);
    crypto1_word(t, nr1_enc, 1);
    if (ar1_enc == (crypto1_word(t, 0, 0) ^ p64b)) {
      break;
    }
  }
  free(s);

  auto returnKey = Nan::New<v8::Number>(key);
  info.GetReturnValue().Set(returnKey);
}

NAN_METHOD(MFKEY64) {
  Isolate* isolate = info.GetIsolate();
  Local<Context> context = isolate->GetCurrentContext();

  for(size_t i = 0; i < 5; i++) {
    if (!info[i]->IsNumber()) {
      Nan::ThrowError("Argument must be a number");
      return;
    }
  }

  uint32_t uid = info[0]->Uint32Value(context).ToChecked();
  uint32_t nt = info[1]->Uint32Value(context).ToChecked();
  uint32_t nr_enc = info[2]->Uint32Value(context).ToChecked();
  uint32_t ar_enc = info[3]->Uint32Value(context).ToChecked();
  uint32_t at_enc = info[4]->Uint32Value(context).ToChecked();

  struct Crypto1State *revstate;
  uint64_t key;     // recovered key
  uint32_t ks2;     // keystream used to encrypt reader response
  uint32_t ks3;     // keystream used to encrypt tag response

  // Generate lfsr successors of the tag challenge
  uint32_t p64 = prng_successor(nt, 64);

  // Extract the keystream from the messages
  ks2 = ar_enc ^ p64;
  ks3 = at_enc ^ prng_successor(p64, 32);

  revstate = lfsr_recovery64(ks2, ks3);

  // Decrypting communication using keystream if presented

  lfsr_rollback_word(revstate, 0, 0);
  lfsr_rollback_word(revstate, 0, 0);
  lfsr_rollback_word(revstate, nr_enc, 1);
  lfsr_rollback_word(revstate, uid ^ nt, 0);
  crypto1_get_lfsr(revstate, &key);
  crypto1_destroy(revstate);

  auto returnKey = Nan::New<v8::Number>(key);
  info.GetReturnValue().Set(returnKey);
}


NAN_MODULE_INIT(Initialize) {
  NAN_EXPORT(target, MFKEY32V2);
  NAN_EXPORT(target, MFKEY32);
  NAN_EXPORT(target, MFKEY64);
}

NODE_MODULE(mfkey, Initialize)
